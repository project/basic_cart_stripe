
# Basic Cart Stripe


## INTRODUCTION

This module adds Stripe payment integration to the order form for Basic Cart.

## REQUIREMENTS

Requires the Basic Cart module and the Stripe module.

## INSTALLATION

To install this module, place it in your sites/all/modules folder and enable it
on the modules page.

## CONFIGURATION

The configuration page for this module is at `admin/config/basic_cart/stripe`.
It has two options:
* A checkbox that controls whether the address is displayed in the 
  Stripe form element.
* A textfield for the charge description that is sent to Stripe.com.
  This field supports tokens, provided that the `token` module has been enabled.

## CREDIT

The Basic Cart Stripe module was originally developed by Jen Lampton of 
Jeneration Web Development (jeneration.com). It is currently developed
and maintained by David Radcliffe of Triplo LLC (triplo.co).
